#syntax=docker/dockerfile:1.4
# Prod image
FROM php:8.2-fpm-alpine

WORKDIR /app/backend

# php extensions installer: https://github.com/mlocati/docker-php-extension-installer
COPY --from=mlocati/php-extension-installer --link /usr/bin/install-php-extensions /usr/local/bin/

# persistent / runtime deps
RUN apk add --no-cache \
		acl \
		fcgi \
		file \
		gettext \
		git \
        nodejs \
        npm


# Install yarn
RUN npm install --global yarn

RUN set -eux; \
    install-php-extensions \
    	intl \
    	zip \
    	apcu \
		opcache

RUN docker-php-ext-install pdo_mysql

COPY --link docker/php/php-fpm.d/zz-docker.conf /usr/local/etc/php-fpm.d/zz-docker.conf
RUN mkdir -p /var/run/php

# https://getcomposer.org/doc/03-cli.md#composer-allow-superuser
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV PATH="${PATH}:/root/.composer/vendor/bin"

COPY --from=composer/composer:2-bin --link /composer /usr/bin/composer

COPY ./backend/ .

RUN composer install
